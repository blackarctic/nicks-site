import React from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

const fetchUsers = async () => {
  const response = await axios.get("/api/users");
  const users = response.data;
  return users;
}

const useUsers = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState<Error>();
  const [users, setUsers] = React.useState<{ id: string, name: string}[]>();
  React.useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        setUsers(await fetchUsers());
      } catch (error) {
        setError(error);
      }
      setIsLoading(false);
    })();
  }, []);
  return { isLoading, error, users };
}

function App() {
  const { users, isLoading, error } = useUsers();
  
  let body;

  if (isLoading) {
    body = 'Loading...';
  } else if (error) {
    body = 'Error';
  } else if (!users || users.length === 0) {
    body = 'No Users';
  } else {
    body = users.map(x => x.name).join(' ');
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{body}</p>
      </header>
    </div>
  );
}

export default App;
